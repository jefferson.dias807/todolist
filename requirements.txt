flake8==3.7.8
Flask==2.2.2
Flask-Cors==3.0.10
Flask-SQLAlchemy==3.0.3
ipdb==0.13
ipython==8.9.0
SQLAlchemy==2.0.2
